/**
 * Class represents static methods, that creates different kinds
 * of sequences based on methods parameters.
 *
 * @author Sergey
 * @since 2018-11-11
 */
public final class Sequence {

    /**
     * Private constructor for a utility class.
     */
    private Sequence() {
    }

    /**
     * Method creates odd numbers sequence based on interval.
     *
     * @param firstNumber first number of interval
     * @param lastNumber  last number of interval
     * @return sequence of numbers based on particular interval
     */
    public static int[] oddNumbers(final int firstNumber,
                                   final int lastNumber) {

        // get first odd
        int newFirstNumber;

        if (firstNumber % 2 == 0) {
            newFirstNumber = firstNumber + 1;
        } else {
            newFirstNumber = firstNumber;
        }


        return fillArrayByItemsThroughOne(newFirstNumber, lastNumber);
    }

    /**
     * Method creates even numbers sequence based on interval.
     *
     * @param firstNumber first number of interval
     * @param lastNumber  last number of interval
     * @return sequence of numbers based on particular interval
     */
    public static int[] evenNumbers(final int firstNumber,
                                    final int lastNumber) {

        int newFirstNumber;

        if (firstNumber % 2 == 0) {
            newFirstNumber = firstNumber;
        } else {
            newFirstNumber = firstNumber + 1;
        }

        return fillArrayByItemsThroughOne(newFirstNumber, lastNumber);
    }

    /**
     * Method returns array on integers based on interval through one.
     *
     * @param firstNumber first number of interval
     * @param lastNumber  last number of interval
     * @return array of integers
     */
    private static int[] fillArrayByItemsThroughOne(final int firstNumber,
                                                    final int lastNumber) {

        // find size of array
        int size = (lastNumber - firstNumber) / 2 + 1;

        int[] array = new int[size];

        for (int i = 0; i < size; i++) {
            array[i] = firstNumber + (i * 2);
        }

        return array;
    }

    /**
     * Returns an array in reverse order.
     *
     * @param array accepts array of integers
     * @return array in reverse order
     */
    public static int[] reverseArray(int[] array) {

        for (int i = 0; i < array.length / 2; i++) {
            int temp = array[i];
            array[i] = array[array.length - i - 1];
            array[array.length - i - 1] = temp;
        }
        return array;
    }

    /**
     * Returns sum of two arrays element wise.
     *
     * @param a1 first array of integers
     * @param a2 second array of integers
     * @return sum of two arrays
     */
    public static int[] summation(final int[] a1, final int[] a2) {
        int length;

        if (a1.length < a2.length) {
            length = a1.length;
        } else {
            length = a2.length;
        }
        int[] result = new int[length];

        for (int i = 0; i < length; i++) {
            result[i] = a1[i] + a2[i];
        }

        return result;
    }

    /**
     * Returns sequence of fibonacci numbers based on
     * two numbers and amount of numbers.
     *
     * @param n1     first number
     * @param n2     second number
     * @param amount amount of numbers
     * @return fibonacci sequence
     */
    public static int[] fibonacci(final int n1,
                                  final int n2,
                                  final int amount) {

        int[] result = new int[amount];
        for (int i = 0; i < result.length; i++) {
            result[i] = Computation.fibonacci(i, n1, n2);
        }
        return result;
    }

}
