package exception;

/**
 * Signals about wrong range.
 *
 * @author Sergey
 * @since 2018-11-16
 */
public class WrongIntervalException extends RuntimeException {

    /**
     * Constructs a WrongIntervalException with the specified
     * detail message. A detail message is a String that describes
     * this particular exception.
     *
     * @param message the detail message.
     */
    public WrongIntervalException(String message) {
        super(message);
    }
}
