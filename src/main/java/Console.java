import java.util.Scanner;

/**
 * Represents class, which responsible is interaction
 * with user by application console.
 *
 * @author Sergey
 * @since 2018-11-11
 */
public class Console {

    /**
     * Simple text scanner used to get the input from console.
     */
    private Scanner sc;

    /**
     * Class constructor creates instance of Scanner.
     */
    public Console() {
        sc = new Scanner(System.in, "UTF-8");
    }

    /**
     * Method that prints text to console.
     *
     * @param text string that should be shown in console
     */
    public final void print(final String text) {
        System.out.println(text);
    }

    /**
     * Method that prints array of integers to console inline.
     *
     * @param array array of integers that should be shown in console
     */
    public final void print(final int[] array) {
        for (int i : array) {
            System.out.print(i + " ");
        }
        System.out.println();
    }

    /**
     * Method that gets users input from console.
     *
     * @return gets input in console via particular method of Scanner class
     */
    public final int getInput() {
        return sc.nextInt();
    }

}
