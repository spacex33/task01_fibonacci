import exception.WrongIntervalException;

/**
 * Class is responsible for the base logic
 * of interaction between user view and computations.
 * Class delegates working with console,
 * sequences and computation to another classes.
 *
 * @author Sergey
 * @since 2018-11-11
 */
public class ConsoleApplication {

    /**
     * Instance variable represents interaction with user.
     * The way of this interaction encapsulated inside class.
     *
     * @see Console
     */
    private Console console = new Console();

    /**
     * Method used as entry point to console interaction,
     * it maintains application in infinity loop
     * and includes main functionality.
     */
    public final void start() {
        Interval inputNumberInterval = askForIntervalInput();

        while (true) {
            showConsoleMenu();
            int menuChoice = console.getInput();

            int[] oddNumbers = getOddNumbersSequence(inputNumberInterval);
            int[] evenNumbers = getEvenNumbersSequence(inputNumberInterval);

            switch (menuChoice) {
                case 1:
                    displayOddNumbers(oddNumbers);
                    break;
                case 2:
                    displayReverseEvenNumbers(evenNumbers);
                    break;
                case 3:
                    displaySumOfNumbers(oddNumbers, evenNumbers);
                    break;
                case 4:
                    int amount =
                            askForInput("Enter amount of numbers: ");
                    int[] fibonacciSequence =
                            buildFibonacciSequenceFromBiggestEvenAndOddNumbers(
                                    oddNumbers, evenNumbers, amount);
                    displayFibonacciSequence(fibonacciSequence);
                    displayOddAndEvenPercentage(fibonacciSequence);
                    break;
                case 0:
                default:
                    System.exit(0);
            }
        }
    }

    /**
     * Method displays menu text.
     */
    private void showConsoleMenu() {
        console.print("1 - Print odd numbers, "
                + "2 - Print even numbers reversely, "
                + "3 - Print sum of odd and even numbers, "
                + "4 - Build Fibonacci number with biggest odd/even numbers, "
                + "0 - exit");
    }

    /**
     * Returns Interval object, based on used input.
     *
     * @return Interval object
     */
    private Interval askForIntervalInput() {
        int firstNumber;
        int lastNumber;

        while (true) {

            firstNumber = askForInput("Enter first number of interval: ");
            lastNumber = askForInput("Enter last number of interval: ");

            try {
                // last number should be larger
                if (lastNumber <= firstNumber) {
                    throw new WrongIntervalException(
                            "Last number should be larger that first");
                }
                break; // breaks infinite loop if exception wasn't thrown

            } catch (WrongIntervalException e) {
                // if exceptions was thrown, print particular message and ask input again
                console.print(e.getMessage());
            }

        }

        return new Interval(firstNumber, lastNumber);
    }

    /**
     * Method used to get user input based on particular request.
     *
     * @param request text that will be displayed to user
     * @return user input
     */
    private int askForInput(final String request) {
        console.print(request);
        return console.getInput();
    }

    /**
     * Returns odd numbers sequence based on Interval object.
     *
     * @param interval Interval object
     * @return odd numbers sequence as array of integers
     */
    private int[] getOddNumbersSequence(final Interval interval) {
        return Sequence.oddNumbers(interval.getFirst(), interval.getLast());
    }

    /**
     * Returns even numbers sequence based on Interval object.
     *
     * @param interval Interval object
     * @return even numbers sequence as array of integers
     */
    private int[] getEvenNumbersSequence(final Interval interval) {
        return Sequence.evenNumbers(interval.getFirst(), interval.getLast());
    }

    /**
     * Builds fibonacci sequence with biggest
     * from even numbers and biggest from odd numbers,
     * fibonacci sequence limited by user typed amount of numbers.
     *
     * @param oddNumbers  odd numbers sequence as array of integers
     * @param evenNumbers even numbers sequence as array of integers
     * @param amount      amount of numbers in fibonacci sequence
     * @return fibonacci sequence as array of integers
     */
    private int[] buildFibonacciSequenceFromBiggestEvenAndOddNumbers(
            final int[] oddNumbers,
            final int[] evenNumbers,
            final int amount
    ) {
        return Sequence.fibonacci(
                oddNumbers[oddNumbers.length - 1],
                evenNumbers[evenNumbers.length - 1],
                amount
        );
    }

    /**
     * Shows sum of odd numbers sequence and even numbers sequence.
     *
     * @param oddNumbers  odd numbers sequence as array of integers
     * @param evenNumbers even numbers sequence as array of integers
     */
    private void displaySumOfNumbers(final int[] oddNumbers,
                                     final int[] evenNumbers) {
        console.print(Sequence.summation(oddNumbers, evenNumbers));
    }

    /**
     * Shows to user odd number sequence.
     *
     * @param oddNumbers odd numbers sequence as array of integers
     */
    private void displayOddNumbers(final int[] oddNumbers) {
        console.print(oddNumbers);
    }

    /**
     * Shows to user even number sequence in reverse order.
     *
     * @param evenNumbers even numbers sequence as array of integers
     */
    private void displayReverseEvenNumbers(final int[] evenNumbers) {
        console.print(Sequence.reverseArray(evenNumbers));
    }

    /**
     * Shows to user fibonacci sequence.
     *
     * @param fibonacciSequence fibonacci sequence as array of integer
     */
    private void displayFibonacciSequence(final int[] fibonacciSequence) {
        console.print(fibonacciSequence);

    }

    /**
     * Shows percentage of odd and even Fibonacci numbers.
     *
     * @param fibonacciSequence fibonacci sequence as array of integer
     */
    private void displayOddAndEvenPercentage(final int[] fibonacciSequence) {
        console.print(Computation.percentEven(fibonacciSequence) + "% even");
        console.print(Computation.percentOdd(fibonacciSequence) + "% odd");
    }

    /**
     * Class that represents interval numbers.
     * Class consists only from constructor
     * that takes two parameters with particular data type
     * and correspond getters.
     * You can't create instance without interval numbers,
     * with only one, or numbers with a different data type,
     * thus this adds some king of implicit validation
     *
     * @author Sergey
     * @since 2018-11-11
     */
    public static class Interval {

        /**
         * First number of interval.
         */
        private int first;

        /**
         * Last number of interval.
         */
        private int last;

        /**
         * Class constructor that takes two integers as interval.
         *
         * @param firstNumber numbers that represents first number of interval
         * @param lastNumber  numbers that represents last number of interval
         */
        Interval(final int firstNumber, final int lastNumber) {
            this.first = firstNumber;
            this.last = lastNumber;
        }

        /**
         * Gets the first number of interval.
         *
         * @return first number of interval
         */
        final int getFirst() {
            return this.first;
        }

        /**
         * Gets the last number of interval.
         *
         * @return last number of interval
         */
        final int getLast() {
            return this.last;
        }
    }

}
