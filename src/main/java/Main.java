/**
 * Main class, that contains an entry point of application.
 *
 * @author Sergey
 * @since 2018-11-11
 */
public final class Main {

    /**
     * Private utility constructor.
     */
    private Main() {
    }

    /**
     * Method that represents an entry point of whole program.
     * Creates an instance of ConsoleApplication and invokes particular method.
     * @param args command line arguments
     */
    public static void main(final String[] args) {
        new ConsoleApplication().start();
    }
}
