/**
 * Class represents static methods to do some computation.
 * based on method parameters and return a result of computation.
 *
 * @author Sergey
 * @since 2018-11-11
 */
public final class Computation {

    /**
     * Private constructor for a utility class.
     */
    private Computation() {
    }

    /**
     * Represents 100 percent.
     */
    private static final double FULL_PERCENT = 100.0;

    /**
     * Method finds fibonacci number in recursive way.
     *
     * @param n      whats number in sequence needs to be returned
     * @param first  first number in a sequence
     * @param second second number in a sequence
     * @return returns fibonacci number on a particular number
     */
    public static int fibonacci(final int n,
                                final int first,
                                final int second) {
        switch (n) {
            case 0:
                return first;
            case 1:
                return second;
            default:
                return fibonacci(n - 1, first, second)
                        + fibonacci(n - 2, first, second);
        }
    }

    /**
     * Returns percent of even numbers in array on integers.
     *
     * @param array array of integers
     * @return percent of even numbers as double
     */
    public static double percentEven(final int[] array) {
        int even = array.length;
        for (int x : array) {
            even -= x & 1;
        }
        return FULL_PERCENT * even / array.length;
    }

    /**
     * Returns percent of odd numbers in array on integers.
     *
     * @param array array of integers
     * @return percent of odd numbers as double
     */
    public static double percentOdd(final int[] array) {
        return FULL_PERCENT - percentEven(array);
    }
}
